package my.net.diary.domain.use.cases

import my.net.diary.domain.entites.Client
import my.net.diary.domain.repositories.IClientRepository

class CreateClientUseCase(private val repository: IClientRepository) {
    suspend fun create(client: Client): Int {
        return repository.create(client)
    }
}