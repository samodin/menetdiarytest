package my.net.diary.domain.use.cases

import my.net.diary.domain.entites.Client
import my.net.diary.domain.repositories.IClientRepository

class EditUseCaseClient(private val repository: IClientRepository) {
    suspend fun edit(client: Client): Boolean {
        return repository.edit(client)
    }
}