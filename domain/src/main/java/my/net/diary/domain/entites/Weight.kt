package my.net.diary.domain.entites

data class Weight(
    private val value: Double,
    val type: WeightType
) {
    fun getWeightKg(): Double {
        return if (type == WeightType.KG) {
            value
        } else {
            convertLbToKg(value)
        }
    }

    fun getWeightLb(): Double {
        return if (type == WeightType.LB) {
            value
        } else {
            convertKgToLb(value)
        }
    }

    fun getWeight() = value

    private fun convertKgToLb(kg: Double): Double {
        return kg * 2.2046
    }

    private fun convertLbToKg(lb: Double): Double {
        return lb / 2.2046
    }

    companion object {
        fun empty(): Weight {
            return Weight(0F.toDouble(), WeightType.KG)
        }
    }
}

enum class WeightType {
    KG,
    LB
}