package my.net.diary.domain.use.cases

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import my.net.diary.domain.entites.Client
import my.net.diary.domain.repositories.IClientRepository

class LoadClientsUseCase(private val repository: IClientRepository) {
    fun loadClients(offset: Long, pageSize: Int): Flow<PagingData<Client>> {
        return repository.get(offset, pageSize, null)
    }

    fun loadClients(): Flow<PagingData<Client>> {
        return repository.get(null)
    }
}