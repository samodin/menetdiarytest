package my.net.diary.domain.repositories

import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import my.net.diary.domain.entites.Client

interface IClientRepository {
    suspend fun create(client: Client): Int

    suspend fun edit(client: Client): Boolean

    fun get(
        offset: Long,
        pageSize: Int,
        pagingConfig: PagingConfig?
    ): Flow<PagingData<Client>>

    fun get(pagingConfig: PagingConfig?): Flow<PagingData<Client>>
}