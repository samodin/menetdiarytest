package my.net.diary.domain.entites

import java.util.*

data class Client(
    var id: Int,
    var weight: Weight,
    var dateOfBirth: Date,
    var photoUrl: String
)