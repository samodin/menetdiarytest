package me.net.diary.data.validators

import java.util.*

class DateBirthValidator(
    private val minAge: Int,
    private val ageLessMinMessage: String,
    emptyMessage: String
) : EmptyValidator(emptyMessage) {
    var dateOfBirth: Long? = null
        set(value) {
            field = value
            super.field = value?.toString()
        }

    override fun validate(): ValidationResult {
        val fieldValue: Long = dateOfBirth ?: 0
        val calendar = Calendar.getInstance()
        calendar.roll(Calendar.YEAR, minAge * (-1))
        calendar.set(Calendar.HOUR, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        if (Date(fieldValue).after(calendar.time)) {
            return INVALID(ageLessMinMessage)
        }
        return super.validate()
    }
}