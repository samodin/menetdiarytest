package me.net.diary.data.validators

interface IValidator {
    fun validate(): ValidationResult
}