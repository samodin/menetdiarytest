package me.net.diary.data.files

import android.net.Uri

interface IFileManager {
    fun copyToInternalDirectory(filePath: String?): String?

    fun removeFileFromInternalDirectory(filePath: String?)

    fun prepareFileForCamera(authority: String): Uri
}