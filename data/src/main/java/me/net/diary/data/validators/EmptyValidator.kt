package me.net.diary.data.validators

abstract class EmptyValidator(
    private val messageError: String
) : IValidator {
    protected var field: String? = null

    override fun validate(): ValidationResult {
        if (field.isNullOrBlank()) {
            return INVALID(messageError)
        }
        return VALID
    }
}