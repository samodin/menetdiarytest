package me.net.diary.data.validators

sealed class ValidationResult
object VALID : ValidationResult()
class INVALID(val message: String) : ValidationResult()