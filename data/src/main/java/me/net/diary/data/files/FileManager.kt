package me.net.diary.data.files

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.core.content.FileProvider
import java.io.File
import java.io.IOException

class FileManager(private val context: Context) : IFileManager {
    override fun copyToInternalDirectory(filePath: String?): String? {
        try {
            filePath ?: return null
            if (ifFileIsInInternalStorage(filePath, context)) {
                return filePath
            }
            val destination = getDestinationPath(filePath)
            copy(filePath, destination)
            return "${context.filesDir}/$destination"
        } catch (e: IOException) {
            Log.e("FileManager", e.message ?: "", e)
        }
        return null
    }

    private fun ifFileIsInInternalStorage(filePath: String, context: Context): Boolean =
        filePath.contains(context.packageName) && !filePath.contains("content://")

    override fun removeFileFromInternalDirectory(filePath: String?) {
        if (filePath.isNullOrEmpty()) return
        File(filePath).deleteOnExit()
    }

    override fun prepareFileForCamera(authority: String): Uri {
        val tmpFile =
            File.createTempFile("tmp_image_file", ".png", context.cacheDir).apply {
                createNewFile()
                deleteOnExit()
            }

        return FileProvider.getUriForFile(
            context,
            authority,
            tmpFile
        )
    }

    private fun getDestinationPath(filePath: String): String {
        return "${System.currentTimeMillis()}.${getFileExtension(filePath)}"
    }

    private fun getFileExtension(filePath: String): String? {
        val uri = Uri.parse(filePath)
        return if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            val mime = MimeTypeMap.getSingleton()
            mime.getExtensionFromMimeType(context.contentResolver.getType(uri))
        } else {
            MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(File(uri.path)).toString())
        }
    }

    @Throws(IOException::class)
    private fun copy(src: String, dst: String) {
        Log.d("FileManager", "src = $src, dst = $dst")
        val inStream = context.contentResolver.openInputStream(Uri.parse(src))
        val outStream = context.openFileOutput(dst, Context.MODE_PRIVATE)
        inStream ?: return
        outStream ?: return
        inStream.copyTo(outStream)
    }
}