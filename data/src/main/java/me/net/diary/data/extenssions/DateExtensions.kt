package me.net.diary.data.extenssions

import java.text.SimpleDateFormat
import java.util.*

fun Date.toDefaultStringFormat() = SimpleDateFormat("MMM dd,yyyy").format(this)
fun Long.toDefaultStringFormat() = SimpleDateFormat("MMM dd,yyyy").format(this)

fun Long?.removeTimeFromDate(): Long? {
    this ?: return null
    val date = Date(this)
    val calendar = Calendar.getInstance()
    calendar.time = date
    calendar.set(Calendar.HOUR, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.time.time
}