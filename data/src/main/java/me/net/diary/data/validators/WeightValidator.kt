package me.net.diary.data.validators

class WeightValidator(
    private val minWeightKg: Double,
    private val maxWeightKg: Double,
    private val weightLessMinMessage: String,
    private val weightBiggerMaxMessage: String,
    emptyMessage: String
) : EmptyValidator(emptyMessage) {
    var weight: Double? = null
        set(value) {
            field = value
            super.field = value?.toString()
        }

    override fun validate(): ValidationResult {
        val fieldValue: Double = weight ?: 0F.toDouble()
        if (fieldValue > maxWeightKg) {
            return INVALID(weightLessMinMessage)
        }
        if (fieldValue < minWeightKg) {
            return INVALID(weightBiggerMaxMessage)
        }
        return super.validate()
    }
}