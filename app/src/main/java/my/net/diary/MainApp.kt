package my.net.diary

import android.app.Application

class MainApp : Application() {
    override fun onCreate() {
        super.onCreate()
        DaggerManager.initAppComponent(this)
    }
}