package my.net.diary.di

import dagger.Module
import dagger.Provides
import me.net.diary.data.validators.DateBirthValidator
import me.net.diary.data.validators.WeightValidator
import my.net.diary.R
import my.net.diary.common.IResourceManager
import javax.inject.Singleton

@Module
class ValidatorModule {

    @Singleton
    @Provides
    fun provideDateBirthValidator(resourceManager: IResourceManager): DateBirthValidator =
        DateBirthValidator(
            resourceManager.getInteger(R.integer.min_age),
            resourceManager.getString(R.string.error_age_is_not_correct),
            resourceManager.getString(R.string.error_field_empty)
        )

    @Singleton
    @Provides
    fun provideWeightValidator(resourceManager: IResourceManager): WeightValidator =
        WeightValidator(
            resourceManager.getInteger(R.integer.min_weight_kg).toDouble(),
            resourceManager.getInteger(R.integer.max_weight_kg).toDouble(),
            resourceManager.getString(R.string.error_weight_is_not_correct),
            resourceManager.getString(R.string.error_weight_is_not_correct),
            resourceManager.getString(R.string.error_field_empty)
        )
}