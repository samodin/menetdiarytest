package my.net.diary.di

import dagger.Component
import me.net.diary.data.files.IFileManager
import me.net.diary.data.validators.DateBirthValidator
import me.net.diary.data.validators.WeightValidator
import my.net.diary.common.IResourceManager
import my.net.diary.domain.use.cases.CreateClientUseCase
import my.net.diary.domain.use.cases.EditUseCaseClient
import my.net.diary.domain.use.cases.LoadClientsUseCase
import javax.inject.Singleton

@Component(
    modules = [
        AppModule::class,
        DbModule::class,
        RepositoryModule::class,
        DomainModule::class,
        ValidatorModule::class,
        DataModule::class]
)
@Singleton
interface AppComponent {
    fun loadClientsUseCase(): LoadClientsUseCase
    fun createClientUseCase(): CreateClientUseCase
    fun editUseCaseClient(): EditUseCaseClient
    fun resourceManager(): IResourceManager
    fun fileManager(): IFileManager

    fun dateBirthValidator(): DateBirthValidator
    fun weightValidator(): WeightValidator
}