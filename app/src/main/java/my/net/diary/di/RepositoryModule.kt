package my.net.diary.di

import dagger.Module
import dagger.Provides
import my.net.diary.db.ClientDao
import my.net.diary.db.ClientRepository
import my.net.diary.domain.repositories.IClientRepository
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideClientRepository(dao: ClientDao): IClientRepository = ClientRepository(dao)
}