package my.net.diary.di

import dagger.Module
import dagger.Provides
import my.net.diary.domain.repositories.IClientRepository
import my.net.diary.domain.use.cases.CreateClientUseCase
import my.net.diary.domain.use.cases.EditUseCaseClient
import my.net.diary.domain.use.cases.LoadClientsUseCase
import javax.inject.Singleton

@Module
class DomainModule {
    @Singleton
    @Provides
    fun provideLoadClientsUseCase(repository: IClientRepository): LoadClientsUseCase =
        LoadClientsUseCase(repository)

    @Singleton
    @Provides
    fun provideEditUseCaseClient(repository: IClientRepository): EditUseCaseClient =
        EditUseCaseClient(repository)

    @Singleton
    @Provides
    fun provideCreateClientUseCase(repository: IClientRepository): CreateClientUseCase =
        CreateClientUseCase(repository)
}