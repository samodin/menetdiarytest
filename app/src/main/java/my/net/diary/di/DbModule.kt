package my.net.diary.di

import android.content.Context
import dagger.Module
import dagger.Provides
import my.net.diary.db.ClientDao
import my.net.diary.db.DataBaseManager
import javax.inject.Singleton

@Module
class DbModule {
    @Singleton
    @Provides
    fun provideDataBaseManager(context: Context) = DataBaseManager(context)

    @Singleton
    @Provides
    fun provideClientDao(dataBaseManager: DataBaseManager): ClientDao =
        dataBaseManager.getClientDao()
}