package my.net.diary.di

import android.content.Context
import dagger.Module
import dagger.Provides
import my.net.diary.MainApp
import my.net.diary.common.IResourceManager
import my.net.diary.common.ResourceManager
import javax.inject.Singleton

@Module
class AppModule(private val app: MainApp) {
    @Provides
    @Singleton
    fun provideApplication(): MainApp = app

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideResourceManager(): IResourceManager = ResourceManager(app)
}