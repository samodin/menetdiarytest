package my.net.diary.di

import dagger.Module
import dagger.Provides
import me.net.diary.data.files.FileManager
import me.net.diary.data.files.IFileManager
import my.net.diary.MainApp
import javax.inject.Singleton

@Module
class DataModule {
    @Singleton
    @Provides
    fun provideFileManager(app: MainApp): IFileManager = FileManager(app)
}