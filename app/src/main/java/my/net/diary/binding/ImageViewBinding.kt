package my.net.diary.binding

import android.net.Uri
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import my.net.diary.R
import java.io.File

@BindingAdapter("loadAvatarImage")
fun ImageView.loadAvatarImage(url: String?) {
    if (url == null) {
        loadAvatar(this, url)
        return
    }
    val uri = Uri.parse(url)
    if (uri.scheme != null) {
        loadAvatar(this, url)
    } else {
        loadAvatar(this, File(url))
    }
}

private fun loadAvatar(imageView: ImageView, file: File) {
    try {
        Picasso.get()
            .load(file)
            .centerCrop()
            .fit()
            .placeholder(R.drawable.ic_no_avatar)
            .into(imageView)
    } catch (e: Exception) {
        Log.e("Picasso", e.message, e)
    }
}

private fun loadAvatar(imageView: ImageView, uri: String?) {
    try {
        Picasso.get()
            .load(uri)
            .centerCrop()
            .fit()
            .placeholder(R.drawable.ic_no_avatar)
            .into(imageView)
    } catch (e: Exception) {
        Log.e("Picasso", e.message, e)
    }
}