package my.net.diary.binding

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("errorMsg")
fun TextInputLayout.setErrorMessage(errorMessage: String?) {
    error = errorMessage
}