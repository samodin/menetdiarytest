package my.net.diary

import my.net.diary.client.changing.date.of.birth.di.AddOrEditClientDateOfBirthComponent
import my.net.diary.client.changing.date.of.birth.di.DaggerAddOrEditClientDateOfBirthComponent
import my.net.diary.client.changing.di.AddOrEditClientComponent
import my.net.diary.client.changing.di.DaggerAddOrEditClientComponent
import my.net.diary.client.changing.photo.di.AddOrEditClientPhotoComponent
import my.net.diary.client.changing.photo.di.DaggerAddOrEditClientPhotoComponent
import my.net.diary.client.changing.weight.di.AddOrEditClientWeightComponent
import my.net.diary.client.changing.weight.di.DaggerAddOrEditClientWeightComponent
import my.net.diary.client.list.di.ClientListComponent
import my.net.diary.client.list.di.DaggerClientListComponent
import my.net.diary.di.AppComponent
import my.net.diary.di.AppModule
import my.net.diary.di.DaggerAppComponent

object DaggerManager {
    lateinit var appComponent: AppComponent
    private var clientListComponent: ClientListComponent? = null
    private var addOrEditClientComponent: AddOrEditClientComponent? = null
    private var addOrEditClientWeightComponent: AddOrEditClientWeightComponent? = null
    private var addOrEditClientDateOfBirthComponent: AddOrEditClientDateOfBirthComponent? = null
    private var addOrEditClientPhotoComponent: AddOrEditClientPhotoComponent? = null

    fun initAppComponent(application: MainApp) {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(application))
            .build()
    }

    fun getClientListComponent(): ClientListComponent {
        return clientListComponent ?: DaggerClientListComponent.builder()
            .appComponent(appComponent)
            .build()
            .apply {
                clientListComponent = this
            }
    }

    fun clearClientListComponent() {
        clientListComponent = null
    }

    fun getAddOrEditClientComponent(): AddOrEditClientComponent {
        return addOrEditClientComponent ?: DaggerAddOrEditClientComponent.builder()
            .appComponent(appComponent)
            .build()
            .apply {
                addOrEditClientComponent = this
            }
    }

    fun clearAddOrEditClientComponent() {
        addOrEditClientComponent = null
    }

    fun getAddOrEditClientWeightComponent(): AddOrEditClientWeightComponent {
        return addOrEditClientWeightComponent ?: DaggerAddOrEditClientWeightComponent.builder()
            .appComponent(appComponent)
            .addOrEditClientComponent(addOrEditClientComponent)
            .build()
            .apply {
                addOrEditClientWeightComponent = this
            }
    }

    fun clearAddOrEditClientWeightComponent() {
        addOrEditClientWeightComponent = null
    }

    fun getAddOrEditClientDateOfBirthComponent(): AddOrEditClientDateOfBirthComponent {
        return addOrEditClientDateOfBirthComponent
            ?: DaggerAddOrEditClientDateOfBirthComponent.builder()
                .appComponent(appComponent)
                .addOrEditClientComponent(addOrEditClientComponent)
                .build()
                .apply {
                    addOrEditClientDateOfBirthComponent = this
                }
    }

    fun clearAddOrEditClientDateOfBirthComponent() {
        addOrEditClientDateOfBirthComponent = null
    }

    fun getAddOrEditClientPhotoComponent(): AddOrEditClientPhotoComponent {
        return addOrEditClientPhotoComponent
            ?: DaggerAddOrEditClientPhotoComponent.builder()
                .appComponent(appComponent)
                .addOrEditClientComponent(addOrEditClientComponent)
                .build()
                .apply {
                    addOrEditClientPhotoComponent = this
                }
    }

    fun clearAddOrEditClientPhotoComponent() {
        addOrEditClientPhotoComponent = null
    }
}