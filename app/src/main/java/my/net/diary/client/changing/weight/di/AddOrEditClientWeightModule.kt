package my.net.diary.client.changing.weight.di

import dagger.Module
import dagger.Provides
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.client.changing.weight.AddOrEditClientWeightViewModelFactory

@Module
class AddOrEditClientWeightModule {

    @AddOrEditClientWeightScope
    @Provides
    fun provideAddOrEditClientWeightViewModelFactory(
        clientData: ClientDataLiveData
    ) = AddOrEditClientWeightViewModelFactory(clientData)
}