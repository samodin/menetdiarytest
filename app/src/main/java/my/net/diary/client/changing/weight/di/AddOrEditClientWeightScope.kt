package my.net.diary.client.changing.weight.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class AddOrEditClientWeightScope