package my.net.diary.client.changing.photo

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import me.net.diary.data.files.IFileManager
import my.net.diary.BuildConfig
import my.net.diary.DaggerManager
import my.net.diary.R
import my.net.diary.client.changing.IValidatedPage
import my.net.diary.databinding.FragmentAddOrEditPhotoBinding
import javax.inject.Inject

class AddOrEditClientPhotoFragment : Fragment(), IValidatedPage {
    private var binding: FragmentAddOrEditPhotoBinding? = null
    private val takeImageResult =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { isSuccess ->
            if (isSuccess) {
                latestTmpUri?.let { uri ->
                    viewModel.photoPath.value = uri.toString()
                }
            }
        }
    private val getContent =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            viewModel.photoPath.value = uri?.toString()
        }

    @Inject
    lateinit var viewModelFactory: AddOrEditClientPhotoViewModelFactory
    private val viewModel: AddOrEditClientPhotoViewModel by viewModels(
        { this },
        { viewModelFactory })

    @Inject
    lateinit var fileManager: IFileManager

    init {
        DaggerManager.getAddOrEditClientPhotoComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddOrEditPhotoBinding.inflate(inflater, container, false)
        binding?.viewModel = viewModel
        binding?.lifecycleOwner = viewLifecycleOwner
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.makePhoto.observe(viewLifecycleOwner, Observer {
            takeImage()
        })
        viewModel.choosePhoto.observe(viewLifecycleOwner, Observer {
            getContent.launch("image/*")
        })
    }

    private var latestTmpUri: Uri? = null

    private fun takeImage() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            getTmpFileUri().let { uri ->
                latestTmpUri = uri
                takeImageResult.launch(uri)
            }
        }
    }

    private fun getTmpFileUri(): Uri {
        return fileManager.prepareFileForCamera("${BuildConfig.APPLICATION_ID}.provider")
    }

    override fun validate(): Boolean {
        return viewModel.validate()
    }

    override fun save(): Boolean {
        return viewModel.save()
    }

    override fun getTitle(): Int {
        return R.string.enter_photo_title
    }

    override fun onDestroyView() {
        DaggerManager.clearAddOrEditClientPhotoComponent()
        binding = null
        super.onDestroyView()
    }
}