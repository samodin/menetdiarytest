package my.net.diary.client.changing

interface IValidatedPage {
    fun validate(): Boolean
    fun save(): Boolean
    fun getTitle(): Int
}