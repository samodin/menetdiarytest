package my.net.diary.client.changing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.activity_main.*
import my.net.diary.DaggerManager
import my.net.diary.MainActivity
import my.net.diary.R
import my.net.diary.base.BaseFragment
import my.net.diary.client.changing.date.of.birth.AddOrEditClientDateOfBirthFragment
import my.net.diary.client.changing.photo.AddOrEditClientPhotoFragment
import my.net.diary.client.changing.view.StepsView
import my.net.diary.client.changing.weight.AddOrEditClientWeightFragment
import my.net.diary.databinding.FragmentAddOrEditClientBinding
import javax.inject.Inject


class AddOrEditClientFragment : BaseFragment() {
    private lateinit var binding: FragmentAddOrEditClientBinding
    private var fragments: List<Fragment>
    private lateinit var adapter: AddOrEditClientAdapter
    private val args: AddOrEditClientFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: AddOrEditClientViewModelFactory
    private val viewModel: AddOrEditClientViewModel by viewModels({ this }, { viewModelFactory })

    init {
        DaggerManager.getAddOrEditClientComponent().inject(this)
        fragments = listOf<Fragment>(
            AddOrEditClientWeightFragment(),
            AddOrEditClientDateOfBirthFragment(),
            AddOrEditClientPhotoFragment()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    handleBackPress()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    private fun handleBackPress() {
        getViewPager().let {
            if (it.currentItem == 0) {
                closeScreen()
            } else {
                it.currentItem--
                binding.steps.setNewStep(it.currentItem)
                setNewTitle()
                setCorrectTextToButton(it)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddOrEditClientBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager()
        initViewModel()
        configListeners()
        args.clientData?.let {
            viewModel.edit(it)
        }
        (requireActivity() as MainActivity).toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        setNewTitle()
        binding.steps.callback = object : StepsView.StepsViewCallback {
            override fun isStepValid(position: Int): Boolean {
                return when (position) {
                    0 -> viewModel.clientData.validateWeightStep()
                    1 -> viewModel.clientData.validateDateOfBirthStep()
                    2 -> viewModel.clientData.validatePhotoStep()
                    else -> false
                }
            }

            override fun openStep(position: Int) {
                openPage(position)
            }
        }
    }

    private fun setCorrectTextToButton(viewPager2: ViewPager2) {
        binding.next.setText(
            if (viewPager2.currentItem == fragments.size - 1) {
                R.string.done
            } else {
                R.string.next
            }
        )
    }

    private fun getViewPager(): ViewPager2 = binding.viewPagerSteps

    private fun save() {
        if (fragments.all { (it as IValidatedPage).validate() }) {
            if ((fragments.last() as IValidatedPage).save()) {
                viewModel.save()
            } else {
                Toast.makeText(requireContext(), R.string.general_error, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun tryToOpenNextPageOrSave() {
        if (!isCurrentPageValid()) return
        getViewPager().let {
            if (it.currentItem == fragments.size - 1) {
                save()
            } else {
                openPage(getCurrentPosition() + 1)
            }
        }
    }

    private fun openPage(position: Int) {
        getViewPager().let {
            if (position >= fragments.size) {
                return@let
            }
            if (position < 0) {
                return@let
            }
            if (getCurrentPosition() > position) {
                getViewPager().currentItem = position
                binding.steps.setNewStep(position)
            } else if (position == getCurrentPosition() + 1
                && isCurrentPageValid()
                && it.currentItem < fragments.size - 1
            ) {
                (adapter.getPage(it.currentItem) as IValidatedPage).save()
                it.currentItem++
                binding.steps.setNewStep(position)
            }
            setNewTitle()
            setCorrectTextToButton(it)
        }
    }

    private fun isPageValid(position: Int): Boolean {
        return (adapter.getPage(position) as IValidatedPage).validate()
    }

    private fun getCurrentPosition(): Int {
        return getViewPager().currentItem
    }

    private fun isCurrentPageValid(): Boolean {
        return isPageValid(getViewPager().currentItem)
    }

    private fun setNewTitle() {
        setTitle((adapter.getPage(getViewPager().currentItem) as IValidatedPage).getTitle())
    }


    private fun configListeners() {
        binding.next.setOnClickListener {
            tryToOpenNextPageOrSave()
        }
    }

    private fun initViewModel() {
        viewModel.close.observe(viewLifecycleOwner, Observer {
            closeScreen()
        })
    }

    private fun closeScreen() {
        navController.navigate(AddOrEditClientFragmentDirections.actionAddOrEditClientFragmentToClientListFragment())
    }

    private fun initViewPager() {
        adapter = AddOrEditClientAdapter(this, fragments)
        binding.viewPagerSteps.adapter = adapter
        binding.viewPagerSteps.isUserInputEnabled = false
    }

    override fun onDestroyView() {
        DaggerManager.clearAddOrEditClientComponent()
        super.onDestroyView()
    }
}