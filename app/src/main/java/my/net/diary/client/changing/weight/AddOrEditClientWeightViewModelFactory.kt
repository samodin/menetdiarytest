package my.net.diary.client.changing.weight

import my.net.diary.base.BaseViewModelFactory
import my.net.diary.client.changing.ClientDataLiveData

class AddOrEditClientWeightViewModelFactory(
    private val clientData: ClientDataLiveData
) :
    BaseViewModelFactory<AddOrEditClientWeightViewModel>(AddOrEditClientWeightViewModel::class.java) {

    override fun createViewModel(): AddOrEditClientWeightViewModel =
        AddOrEditClientWeightViewModel(clientData)
}