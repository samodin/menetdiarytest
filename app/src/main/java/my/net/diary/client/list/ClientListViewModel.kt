package my.net.diary.client.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import my.net.diary.client.changing.ClientData
import my.net.diary.common.SingleLiveEvent
import my.net.diary.domain.entites.Client
import my.net.diary.domain.use.cases.LoadClientsUseCase

class ClientListViewModel(private val useCase: LoadClientsUseCase) : ViewModel() {
    val addOrEditClient = SingleLiveEvent<ClientData?>()

    fun loadAllClients(): Flow<PagingData<Client>> {
        return useCase.loadClients().cachedIn(viewModelScope)
    }

    fun addClient() {
        addOrEditClient.value = null
    }

    fun edit(client: Client) {
        addOrEditClient.value = ClientData.create(client)
    }
}