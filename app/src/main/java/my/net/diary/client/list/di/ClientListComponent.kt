package my.net.diary.client.list.di

import dagger.Component
import my.net.diary.client.list.ClientListFragment
import my.net.diary.di.AppComponent

@ClientListScope
@Component(
    dependencies = [AppComponent::class],
    modules = [ClientListModule::class]
)
interface ClientListComponent {
    fun inject(fragment: ClientListFragment)
}