package my.net.diary.client.changing.date.of.birth.di

import dagger.Module
import dagger.Provides
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.client.changing.date.of.birth.AddOrEditClientDateOfBirthViewModelFactory

@Module
class AddOrEditClientDateOfBirthModule {

    @AddOrEditClientDateOfBirthScope
    @Provides
    fun provideAddOrEditClientDateOfBirthViewModelFactory(
        clientData: ClientDataLiveData
    ) = AddOrEditClientDateOfBirthViewModelFactory(clientData)
}