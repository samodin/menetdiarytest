package my.net.diary.client.changing.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import my.net.diary.DaggerManager
import my.net.diary.R
import my.net.diary.client.changing.IValidatedPage
import my.net.diary.databinding.FragmentAddOrEditWeightBinding
import my.net.diary.domain.entites.WeightType
import javax.inject.Inject

class AddOrEditClientWeightFragment : Fragment(), IValidatedPage {
    private var binding: FragmentAddOrEditWeightBinding? = null

    @Inject
    lateinit var viewModelFactory: AddOrEditClientWeightViewModelFactory
    private val viewModel: AddOrEditClientWeightViewModel by viewModels(
        { this },
        { viewModelFactory })

    init {
        DaggerManager.getAddOrEditClientWeightComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddOrEditWeightBinding.inflate(inflater, container, false)
        binding?.viewModel = viewModel
        binding?.lifecycleOwner = viewLifecycleOwner
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initDropdown()
    }

    private fun initDropdown() {
        val items = listOf(WeightType.KG.name, WeightType.LB.name)
        val adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_expandable_list_item_1, items)
        binding?.weightType?.setAdapter(adapter)
        binding?.weightType?.setOnItemClickListener { parent, view, position, id ->
            viewModel.weightUnit.value = binding?.weightType?.adapter?.getItem(position).toString()
        }
        viewModel.weightUnit.observe(viewLifecycleOwner, Observer {
            binding?.weightType?.setText(it, false)
        })
    }

    override fun validate(): Boolean {
        return viewModel.validate()
    }

    override fun save(): Boolean {
        return viewModel.save()
    }

    override fun getTitle(): Int {
        return R.string.enter_weight_title
    }

    override fun onDestroyView() {
        DaggerManager.clearAddOrEditClientWeightComponent()
        binding = null
        super.onDestroyView()
    }
}