package my.net.diary.client.list

import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import me.net.diary.data.extenssions.toDefaultStringFormat
import my.net.diary.R
import my.net.diary.databinding.ItemClientBinding
import my.net.diary.domain.entites.Client
import java.io.File

class ClientViewHolder(
    private val binding: ItemClientBinding,
    private val actions: ClientItemActions
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(client: Client) {
        binding.weight.text = "${client.weight.getWeight()}${client.weight.type.name.lowercase()}"
        binding.dateOfBirth.text = client.dateOfBirth.toDefaultStringFormat()
        binding.edit.setOnClickListener {
            actions.edit(client)
        }
        Picasso.get().cancelRequest(binding.userAvatar)
        Picasso.get()
            .load(File(client.photoUrl))
            .placeholder(R.drawable.ic_no_avatar)
            .fit()
            .centerCrop()
            .into(binding.userAvatar)
    }
}