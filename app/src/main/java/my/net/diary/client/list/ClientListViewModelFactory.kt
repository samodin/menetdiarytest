package my.net.diary.client.list

import my.net.diary.base.BaseViewModelFactory
import my.net.diary.domain.use.cases.LoadClientsUseCase

class ClientListViewModelFactory(private val useCase: LoadClientsUseCase) :
    BaseViewModelFactory<ClientListViewModel>(ClientListViewModel::class.java) {

    override fun createViewModel(): ClientListViewModel = ClientListViewModel(useCase)
}