package my.net.diary.client.changing.photo.di

import dagger.Module
import dagger.Provides
import me.net.diary.data.files.IFileManager
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.client.changing.photo.AddOrEditClientPhotoViewModelFactory

@Module
class AddOrEditClientPhotoModule {

    @AddOrEditClientPhotoScope
    @Provides
    fun provideAddOrEditClientPhotoViewModelFactory(
        clientData: ClientDataLiveData,
        fileManager: IFileManager
    ) = AddOrEditClientPhotoViewModelFactory(clientData, fileManager)
}