package my.net.diary.client.changing

import my.net.diary.base.BaseViewModelFactory
import my.net.diary.domain.use.cases.CreateClientUseCase
import my.net.diary.domain.use.cases.EditUseCaseClient

class AddOrEditClientViewModelFactory(
    private val clientData: ClientDataLiveData,
    private val createClientUseCase: CreateClientUseCase,
    private val editUseCaseClient: EditUseCaseClient,
) : BaseViewModelFactory<AddOrEditClientViewModel>(AddOrEditClientViewModel::class.java) {

    override fun createViewModel(): AddOrEditClientViewModel =
        AddOrEditClientViewModel(clientData, createClientUseCase, editUseCaseClient)
}