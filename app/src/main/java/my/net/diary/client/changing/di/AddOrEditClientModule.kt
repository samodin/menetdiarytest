package my.net.diary.client.changing.di

import dagger.Module
import dagger.Provides
import me.net.diary.data.validators.DateBirthValidator
import me.net.diary.data.validators.WeightValidator
import my.net.diary.client.changing.AddOrEditClientViewModelFactory
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.domain.use.cases.CreateClientUseCase
import my.net.diary.domain.use.cases.EditUseCaseClient

@Module
class AddOrEditClientModule {
    @AddOrEditClientScope
    @Provides
    fun provideClientDataLiveData(
        weightValidator: WeightValidator,
        dateBirthValidator: DateBirthValidator
    ) = ClientDataLiveData(weightValidator, dateBirthValidator)

    @AddOrEditClientScope
    @Provides
    fun provideAddOrEditClientViewModelFactory(
        clientData: ClientDataLiveData,
        createClientUseCase: CreateClientUseCase,
        editUseCaseClient: EditUseCaseClient
    ) = AddOrEditClientViewModelFactory(clientData, createClientUseCase, editUseCaseClient)
}