package my.net.diary.client.changing.date.of.birth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import my.net.diary.DaggerManager
import my.net.diary.R
import my.net.diary.client.changing.IValidatedPage
import my.net.diary.databinding.FragmentAddOrEditDateOfBirthBinding
import java.util.*
import javax.inject.Inject

class AddOrEditClientDateOfBirthFragment : Fragment(), IValidatedPage {
    private var binding: FragmentAddOrEditDateOfBirthBinding? = null

    @Inject
    lateinit var viewModelFactory: AddOrEditClientDateOfBirthViewModelFactory
    private val viewModel: AddOrEditClientDateOfBirthViewModel by viewModels(
        { this },
        { viewModelFactory })

    init {
        DaggerManager.getAddOrEditClientDateOfBirthComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddOrEditDateOfBirthBinding.inflate(inflater, container, false)
        binding?.viewModel = viewModel
        binding?.lifecycleOwner = viewLifecycleOwner
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.showDateDialog.observe(viewLifecycleOwner, Observer {
            val constraintsBuilder = CalendarConstraints.Builder()
            val calendar = Calendar.getInstance()
            calendar.set(1900, Calendar.JANUARY, 1)
            constraintsBuilder.setStart(calendar.timeInMillis)
            calendar.timeInMillis = System.currentTimeMillis()
            calendar.roll(
                Calendar.YEAR,
                requireContext().resources.getInteger(R.integer.min_age) * (-1)
            )
            constraintsBuilder.setEnd(calendar.timeInMillis)
            constraintsBuilder.setOpenAt(calendar.timeInMillis)
            val builder: MaterialDatePicker.Builder<Long> = MaterialDatePicker.Builder.datePicker()
            builder.setCalendarConstraints(constraintsBuilder.build())
            builder.setSelection(calendar.timeInMillis)
            val picker: MaterialDatePicker<*> = builder.build()
            picker.addOnPositiveButtonClickListener {
                val date = Date(it as Long)
                viewModel.setDateOfBirth(date.time)
            }
            picker.show(childFragmentManager, picker.toString())
        })
        val calendar = Calendar.getInstance()
        calendar.roll(
            Calendar.YEAR,
            requireContext().resources.getInteger(R.integer.min_age) * (-1)
        )
        viewModel.setDateOfBirth(calendar.timeInMillis)
    }

    override fun validate(): Boolean {
        return viewModel.validate()
    }

    override fun save(): Boolean {
        return viewModel.save()
    }

    override fun getTitle(): Int {
        return R.string.enter_date_of_birth_title
    }

    override fun onDestroyView() {
        DaggerManager.clearAddOrEditClientDateOfBirthComponent()
        binding = null
        super.onDestroyView()
    }
}