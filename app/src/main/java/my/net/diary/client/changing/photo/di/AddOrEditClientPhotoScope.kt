package my.net.diary.client.changing.photo.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class AddOrEditClientPhotoScope
