package my.net.diary.client.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_client_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import my.net.diary.DaggerManager
import my.net.diary.base.BaseFragment
import my.net.diary.databinding.FragmentClientListBinding
import javax.inject.Inject

class ClientListFragment : BaseFragment() {
    private var binding: FragmentClientListBinding? = null

    @Inject
    lateinit var viewModelFactory: ClientListViewModelFactory
    private val viewModel: ClientListViewModel by viewModels({ this }, { viewModelFactory })

    private val adapter = ClientAdapter(ClientItemActions { client -> viewModel.edit(client) })

    init {
        DaggerManager.getClientListComponent().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().finish()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentClientListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        configList()
        setListeners()
        initViewModel()
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                viewModel.loadAllClients().collectLatest { adapter.submitData(it) }
            }
        }
    }

    private fun initViewModel() {
        viewModel.addOrEditClient.observe(viewLifecycleOwner, Observer {
            navController.navigate(
                ClientListFragmentDirections.actionClientListFragmentToAddOrEditClientFragment(
                    it
                )
            )
        })
    }

    private fun setListeners() {
        binding?.addClient?.setOnClickListener {
            viewModel.addClient()
        }
    }

    private fun configList() {
        client_list.adapter = adapter
        client_list.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onDestroy() {
        DaggerManager.clearClientListComponent()
        super.onDestroy()
        binding = null
    }
}