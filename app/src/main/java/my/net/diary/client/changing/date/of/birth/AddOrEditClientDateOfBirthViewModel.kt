package my.net.diary.client.changing.date.of.birth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.net.diary.data.extenssions.removeTimeFromDate
import me.net.diary.data.extenssions.toDefaultStringFormat
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.common.SingleLiveEvent
import java.util.*

class AddOrEditClientDateOfBirthViewModel(
    private val clientData: ClientDataLiveData
) : ViewModel() {
    val errorMessage = MutableLiveData<String?>()
    val showDateDialog = SingleLiveEvent<Unit>()
    val dateOfBirth = MutableLiveData<String>()
    private var dateTicks: Long = 0

    init {
        clientData.observeForever {
            dateOfBirth.value = it.dateOfBirth.toDefaultStringFormat()
            dateTicks = it.dateOfBirth.time
        }
    }

    fun showDateDialog() {
        showDateDialog.value = Unit
    }

    fun setDateOfBirth(time: Long) {
        dateTicks = time
        dateOfBirth.value = time.toDefaultStringFormat()
        clientData.value?.dateOfBirth = Date(time)
    }

    fun save(): Boolean {
        if (validate()) {
            clientData.value?.dateOfBirth = Date(dateTicks)
            return true
        }
        return false
    }

    fun validate(): Boolean {
        return clientData.validateDateOfBirthStep(dateTicks.removeTimeFromDate()) {
            errorMessage.value = it
        }
    }
}