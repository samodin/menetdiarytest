package my.net.diary.client.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import my.net.diary.databinding.ItemClientBinding
import my.net.diary.domain.entites.Client

open class ClientAdapter(private val actions: ClientItemActions) :
    PagingDataAdapter<Client, ClientViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder =
        ClientViewHolder(
            ItemClientBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            actions
        )

    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    companion object {
        val diffCallback = ClientDiffUtilCallback()
    }
}