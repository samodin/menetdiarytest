package my.net.diary.client.list

import androidx.recyclerview.widget.DiffUtil
import my.net.diary.domain.entites.Client

class ClientDiffUtilCallback : DiffUtil.ItemCallback<Client>() {

    override fun areItemsTheSame(oldItem: Client, newItem: Client): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Client, newItem: Client): Boolean {
        return (oldItem.dateOfBirth == newItem.dateOfBirth
                && oldItem.photoUrl == newItem.photoUrl
                && oldItem.weight == newItem.weight)
    }
}