package my.net.diary.client.changing.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import my.net.diary.R
import my.net.diary.databinding.ViewStepsBinding

class StepsView(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    interface StepsViewCallback {
        fun isStepValid(position: Int): Boolean
        fun openStep(position: Int)
    }

    class Step(
        val position: Int,
        var isSelected: Boolean,
        val imageView: ImageView,
        val selectedDigitId: Int,
        val unSelectedDigitId: Int
    )

    private var binding: ViewStepsBinding =
        ViewStepsBinding.inflate(LayoutInflater.from(context), this, true)
    private val steps: List<Step> = listOf(
        Step(
            0,
            true,
            binding.stepOne,
            R.drawable.ic_one_selected,
            R.drawable.ic_one_selected
        ),
        Step(
            1,
            false,
            binding.stepTwo,
            R.drawable.ic_two_selected,
            R.drawable.ic_two_unselect
        ),
        Step(
            2,
            false,
            binding.stepThree,
            R.drawable.ic_three_selected,
            R.drawable.ic_three_unselect
        )
    )

    init {
        steps.forEach { step ->
            step.imageView.setOnClickListener {
                openStep(step.position)
            }
        }
    }

    var callback: StepsViewCallback? = null
    var currentStep: Step = steps.first()

    fun setNewStep(position: Int) {
        currentStep = steps[position]
        updateUI(position)
    }

    fun openStep(position: Int) {
        updateUI(position)
        callback?.openStep(position)
    }

    private fun updateUI(position: Int) {
        if ((steps.firstOrNull { it.isSelected } ?: -1) == position) {
            return
        }
        if (callback?.isStepValid(currentStep.position) != true) {
            return
        }
        if (position - currentStep.position >= 2 && currentStep.position <= position) {
            return
        }
        currentStep = steps[position]
        currentStep.run {
            isSelected = true
            imageView.setImageResource(selectedDigitId)
        }
        for (i in 0 until position) {
            steps[i].run {
                isSelected = false
                imageView.setImageResource(R.drawable.ic_step_completed)
            }
        }
        for (i in position + 1 until steps.size) {
            steps[i].run {
                isSelected = false
                imageView.setImageResource(unSelectedDigitId)
            }
        }
    }
}

