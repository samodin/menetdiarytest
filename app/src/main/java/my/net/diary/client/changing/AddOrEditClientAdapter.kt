package my.net.diary.client.changing

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class AddOrEditClientAdapter(
    parent: Fragment,
    private val pages: List<Fragment>
) : FragmentStateAdapter(parent) {

    override fun getItemCount(): Int = pages.size

    override fun createFragment(position: Int): Fragment {
        return getPage(position) as Fragment
    }

    fun getPage(position: Int): Fragment = pages[position]

    fun getPages() = pages

    fun getPagePosition(pageFragment: Fragment) = pages.indexOf(pageFragment)

    override fun getItemId(position: Int): Long {
        return pages[position].hashCode().toLong()
    }

    override fun containsItem(itemId: Long): Boolean {
        return pages.firstOrNull { it.hashCode().toLong() == itemId } != null
    }
}
