package my.net.diary.client.changing.photo

import me.net.diary.data.files.IFileManager
import my.net.diary.base.BaseViewModelFactory
import my.net.diary.client.changing.ClientDataLiveData

class AddOrEditClientPhotoViewModelFactory(
    private val clientData: ClientDataLiveData,
    private val fileManager: IFileManager
) : BaseViewModelFactory<AddOrEditClientPhotoViewModel>(AddOrEditClientPhotoViewModel::class.java) {

    override fun createViewModel(): AddOrEditClientPhotoViewModel =
        AddOrEditClientPhotoViewModel(clientData, fileManager)
}