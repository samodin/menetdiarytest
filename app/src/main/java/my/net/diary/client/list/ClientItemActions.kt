package my.net.diary.client.list

import my.net.diary.domain.entites.Client

class ClientItemActions(val edit: (client: Client) -> Unit)