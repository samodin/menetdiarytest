package my.net.diary.client.changing.date.of.birth

import my.net.diary.base.BaseViewModelFactory
import my.net.diary.client.changing.ClientDataLiveData

class AddOrEditClientDateOfBirthViewModelFactory(
    private val clientData: ClientDataLiveData
) :
    BaseViewModelFactory<AddOrEditClientDateOfBirthViewModel>(AddOrEditClientDateOfBirthViewModel::class.java) {

    override fun createViewModel(): AddOrEditClientDateOfBirthViewModel =
        AddOrEditClientDateOfBirthViewModel(clientData)
}