package my.net.diary.client.list.di

import dagger.Module
import dagger.Provides
import my.net.diary.client.list.ClientListViewModelFactory
import my.net.diary.domain.use.cases.LoadClientsUseCase

@Module
class ClientListModule {

    @ClientListScope
    @Provides
    fun provideClientListViewModelFactory(useCase: LoadClientsUseCase) =
        ClientListViewModelFactory(useCase)
}