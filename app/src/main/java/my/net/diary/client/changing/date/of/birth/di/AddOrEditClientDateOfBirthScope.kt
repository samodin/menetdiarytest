package my.net.diary.client.changing.date.of.birth.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class AddOrEditClientDateOfBirthScope