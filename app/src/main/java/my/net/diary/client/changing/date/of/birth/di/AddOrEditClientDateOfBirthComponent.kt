package my.net.diary.client.changing.date.of.birth.di

import dagger.Component
import my.net.diary.client.changing.date.of.birth.AddOrEditClientDateOfBirthFragment
import my.net.diary.client.changing.di.AddOrEditClientComponent
import my.net.diary.di.AppComponent

@AddOrEditClientDateOfBirthScope
@Component(
    dependencies = [AddOrEditClientComponent::class, AppComponent::class],
    modules = [AddOrEditClientDateOfBirthModule::class]
)

interface AddOrEditClientDateOfBirthComponent {

    fun inject(fragment: AddOrEditClientDateOfBirthFragment)
}