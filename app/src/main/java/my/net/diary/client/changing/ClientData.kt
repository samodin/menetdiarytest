package my.net.diary.client.changing

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import my.net.diary.domain.entites.Client
import my.net.diary.domain.entites.Weight
import java.util.*

@Parcelize
class ClientData : Parcelable {
    var id: Int = 0
    var weight: Weight = Weight.empty()
    var dateOfBirth: Date = Date(0)
    var photoUrl: String = ""

    fun createClient(): Client {
        return Client(id, weight, dateOfBirth, photoUrl)
    }

    companion object {
        fun create(client: Client): ClientData {
            val clientData = ClientData()
            clientData.dateOfBirth = client.dateOfBirth
            clientData.id = client.id
            clientData.weight = client.weight
            return clientData
        }
    }
}