package my.net.diary.client.changing.photo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.net.diary.data.files.IFileManager
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.common.SingleLiveEvent

class AddOrEditClientPhotoViewModel(
    private val clientData: ClientDataLiveData,
    private val fileManager: IFileManager
) : ViewModel() {

    val photoPath = MutableLiveData<String?>()
    val makePhoto = SingleLiveEvent<Unit>()
    val choosePhoto = SingleLiveEvent<Unit>()

    init {
        clientData.observeForever {
            photoPath.value = it.photoUrl
        }
    }

    fun makePhoto() {
        makePhoto.value = Unit
    }

    fun choosePhoto() {
        choosePhoto.value = Unit
    }

    fun save(): Boolean {
        if (validate()) {
            clientData.value?.photoUrl = photoPath.value ?: ""
            clientData.value?.photoUrl =
                fileManager.copyToInternalDirectory(clientData.value?.photoUrl) ?: ""
            if (clientData.value?.photoUrl.isNullOrEmpty()) return false
            if (clientData.oldClientData?.photoUrl != clientData.value?.photoUrl) {
                fileManager.removeFileFromInternalDirectory(
                    clientData.oldClientData?.photoUrl ?: ""
                )
            }
            return true
        }
        return false
    }

    fun validate(): Boolean {
        return clientData.validatePhotoStep(photoPath.value)
    }
}