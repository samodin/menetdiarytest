package my.net.diary.client.changing.di

import dagger.Component
import my.net.diary.client.changing.AddOrEditClientFragment
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.di.AppComponent

@AddOrEditClientScope
@Component(
    dependencies = [AppComponent::class],
    modules = [AddOrEditClientModule::class]
)
interface AddOrEditClientComponent {
    fun clientDataLiveData(): ClientDataLiveData

    fun inject(fragment: AddOrEditClientFragment)
}