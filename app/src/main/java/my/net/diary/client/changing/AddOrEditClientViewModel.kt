package my.net.diary.client.changing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import my.net.diary.common.SingleLiveEvent
import my.net.diary.domain.entites.Client
import my.net.diary.domain.use.cases.CreateClientUseCase
import my.net.diary.domain.use.cases.EditUseCaseClient

class AddOrEditClientViewModel(
    val clientData: ClientDataLiveData,
    private val createClientUseCase: CreateClientUseCase,
    private val editUseCaseClient: EditUseCaseClient,
) : ViewModel() {

    val close = SingleLiveEvent<Unit>()

    fun edit(clientData: ClientData) {
        this.clientData.value = clientData
        this.clientData.oldClientData = clientData
    }

    fun save() {
        viewModelScope.launch {
            createClient()?.let {
                withContext(Dispatchers.IO) {
                    if (clientData.value?.id != 0) {
                        editUseCaseClient.edit(it)
                    } else {
                        createClientUseCase.create(it)
                    }
                }
                close.value = Unit
            }
        }
    }

    private fun createClient(): Client? {
        return clientData.value?.createClient()
    }
}