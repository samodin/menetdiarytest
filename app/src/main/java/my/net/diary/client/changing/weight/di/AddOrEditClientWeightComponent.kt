package my.net.diary.client.changing.weight.di

import dagger.Component
import my.net.diary.client.changing.di.AddOrEditClientComponent
import my.net.diary.client.changing.weight.AddOrEditClientWeightFragment
import my.net.diary.di.AppComponent

@AddOrEditClientWeightScope
@Component(
    dependencies = [AddOrEditClientComponent::class, AppComponent::class],
    modules = [AddOrEditClientWeightModule::class]
)

interface AddOrEditClientWeightComponent {
    fun inject(fragment: AddOrEditClientWeightFragment)
}