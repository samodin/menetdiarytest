package my.net.diary.client.changing.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class AddOrEditClientScope