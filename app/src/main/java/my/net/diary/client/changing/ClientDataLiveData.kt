package my.net.diary.client.changing

import androidx.lifecycle.MutableLiveData
import me.net.diary.data.extenssions.removeTimeFromDate
import me.net.diary.data.validators.DateBirthValidator
import me.net.diary.data.validators.INVALID
import me.net.diary.data.validators.VALID
import me.net.diary.data.validators.WeightValidator
import my.net.diary.domain.entites.Weight

class ClientDataLiveData(
    private val weightValidator: WeightValidator,
    private val dateBirthValidator: DateBirthValidator
) : MutableLiveData<ClientData>() {
    var oldClientData: ClientData? = null

    fun validateWeightStep(
        weightValidation: Weight? = null,
        errorMessage: (message: String?) -> Unit = {}
    ): Boolean {
        return try {
            val validationResult = weightValidator.apply {
                val weightKg = weightValidation ?: value?.weight ?: return false
                weight = weightKg.getWeightKg()
            }.validate()
            when (validationResult) {
                is VALID -> {
                    errorMessage(null)
                    true
                }
                is INVALID -> {
                    errorMessage(validationResult.message)
                    false
                }
                else -> {
                    false
                }
            }
        } catch (e: NumberFormatException) {
            false
        }
    }

    fun validateDateOfBirthStep(
        ticks: Long? = null,
        errorMessage: (message: String?) -> Unit = {}
    ): Boolean {
        return try {
            val validationResult = dateBirthValidator.apply {
                dateOfBirth =
                    ticks.removeTimeFromDate() ?: value?.dateOfBirth?.time.removeTimeFromDate()
                            ?: return false
            }.validate()
            when (validationResult) {
                is VALID -> {
                    errorMessage(null)
                    true
                }
                is INVALID -> {
                    errorMessage(validationResult.message)
                    false
                }
                else -> {
                    false
                }
            }
        } catch (e: NumberFormatException) {
            false
        }
    }

    fun validatePhotoStep(filePath: String? = null): Boolean {
        if (filePath ?: value?.photoUrl != null) {
            return true
        }
        return false
    }
}