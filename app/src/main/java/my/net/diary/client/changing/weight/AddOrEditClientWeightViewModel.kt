package my.net.diary.client.changing.weight

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import my.net.diary.client.changing.ClientData
import my.net.diary.client.changing.ClientDataLiveData
import my.net.diary.domain.entites.Weight
import my.net.diary.domain.entites.WeightType

class AddOrEditClientWeightViewModel(
    private val clientData: ClientDataLiveData
) : ViewModel() {
    val weight = MutableLiveData<String>()
    val errorMessage = MutableLiveData<String?>()
    val weightUnit = MutableLiveData<String?>()

    init {
        if (clientData.value == null) {
            clientData.value = ClientData()
        }
        clientData.observeForever {
            weightUnit.value = it.weight.type.name
            if (it.weight.getWeight() == 0F.toDouble()) {
                weight.value = ""
            } else {
                weight.value = it.weight.getWeight().toString()
            }
        }
        weight.observeForever {
            clientData.value?.weight = createWeight()
        }
    }

    fun validate(): Boolean {
        return try {
            clientData.validateWeightStep(createWeight()) {
                errorMessage.value = it
            }
        } catch (e: java.lang.NumberFormatException) {
            false
        }
    }

    private fun createWeight(): Weight {
        val value = if (this@AddOrEditClientWeightViewModel.weight.value.isNullOrEmpty()) {
            0f.toDouble()
        } else {
            this@AddOrEditClientWeightViewModel.weight.value?.toDouble() ?: 0F.toDouble()
        }
        return Weight(
            value,
            getWeightType()
        )
    }

    private fun getWeightType(): WeightType {
        return if (weightUnit.value == WeightType.KG.name) {
            WeightType.KG
        } else {
            WeightType.LB
        }
    }

    fun save(): Boolean {
        if (validate()) {
            clientData.value?.weight = createWeight()
            return true
        }
        return false
    }
}