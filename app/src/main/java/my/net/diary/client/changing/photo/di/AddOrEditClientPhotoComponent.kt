package my.net.diary.client.changing.photo.di

import dagger.Component
import my.net.diary.client.changing.di.AddOrEditClientComponent
import my.net.diary.client.changing.photo.AddOrEditClientPhotoFragment
import my.net.diary.di.AppComponent

@AddOrEditClientPhotoScope
@Component(
    dependencies = [AddOrEditClientComponent::class, AppComponent::class],
    modules = [AddOrEditClientPhotoModule::class]
)
interface AddOrEditClientPhotoComponent {
    fun inject(fragment: AddOrEditClientPhotoFragment)
}