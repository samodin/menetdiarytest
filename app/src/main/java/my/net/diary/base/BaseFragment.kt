package my.net.diary.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.appbar.MaterialToolbar
import kotlinx.android.synthetic.main.activity_main.*
import my.net.diary.MainActivity
import my.net.diary.R

open class BaseFragment : Fragment() {
    protected lateinit var navController: NavController

    protected fun setTitle(title: String) {
        getActionBar()?.title = title
    }

    protected fun setTitle(resId: Int) {
        getActionBar()?.setTitle(resId)
    }

    private fun getActionBar(): MaterialToolbar? =
        (requireActivity() as MainActivity).toolbar

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController =
            (requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
    }
}