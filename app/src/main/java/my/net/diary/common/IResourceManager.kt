package my.net.diary.common

interface IResourceManager {
    fun getString(id: Int): String
    fun getInteger(id: Int): Int
}