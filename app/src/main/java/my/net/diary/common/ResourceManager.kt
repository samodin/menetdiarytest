package my.net.diary.common

import android.content.Context

class ResourceManager(private val context: Context) : IResourceManager {
    override fun getString(id: Int): String {
        return context.getString(id)
    }

    override fun getInteger(id: Int): Int {
        return context.resources.getInteger(id)
    }
}