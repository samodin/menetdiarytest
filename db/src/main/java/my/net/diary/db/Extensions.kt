package my.net.diary.db

import my.net.diary.domain.entites.Client
import my.net.diary.domain.entites.Weight
import my.net.diary.domain.entites.WeightType
import java.util.*

fun Client.toDbModel(): my.net.diary.db.Client =
    Client(id, weight.getWeight(), weight.type.name, dateOfBirth.toTicks(), photoUrl)

fun my.net.diary.db.Client.toDomainModel(): Client =
    Client(id, Weight(weight, WeightType.valueOf(weightUnit)), dateOfBirth.toDate(), photoUrl)

fun Date.toTicks(): Long {
    return this.time
}

fun Long.toDate(): Date {
    return Date(this)
}