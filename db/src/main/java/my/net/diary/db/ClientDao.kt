package my.net.diary.db

import androidx.room.*


@Dao
interface ClientDao {
    companion object {
        const val TABLE_NAME = "clients"
    }

    @Query("SELECT * FROM $TABLE_NAME")
    suspend fun getAll(): List<Client>

    @Query("SELECT * FROM $TABLE_NAME LIMIT :pageSize OFFSET :offset")
    suspend fun get(offset: Long, pageSize: Int): List<Client>

    @Query("SELECT * FROM $TABLE_NAME WHERE id IN (:ids)")
    suspend fun getAllByIds(ids: Array<String>): List<Client>

    @Query("SELECT * FROM $TABLE_NAME WHERE id = :id")
    suspend fun getAllById(id: Int): List<Client>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(Client: Client): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg client: Client): LongArray

    @Update
    fun update(client: Client): Int

    @Delete
    suspend fun delete(client: Client)

    @Query("DELETE FROM $TABLE_NAME WHERE id = :id")
    suspend fun deleteById(id: String): Int
}