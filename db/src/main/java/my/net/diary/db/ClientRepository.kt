package my.net.diary.db

import androidx.paging.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import my.net.diary.domain.entites.Client
import my.net.diary.domain.repositories.IClientRepository
import java.io.IOException

class ClientRepository(private val dao: ClientDao) : IClientRepository {
    private fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(pageSize = 8, enablePlaceholders = true)
    }

    override suspend fun create(client: Client): Int {
        return dao.insert(client.toDbModel()).toInt()
    }

    override suspend fun edit(client: Client): Boolean {
        return dao.update(client.toDbModel()) > 0
    }

    override fun get(
        offset: Long,
        pageSize: Int,
        pagingConfig: PagingConfig?
    ): Flow<PagingData<Client>> {
        val pagingSourceFactory = { ClientPagingSource(dao) }
        return Pager(
            config = pagingConfig ?: getDefaultPageConfig(),
            pagingSourceFactory = pagingSourceFactory
        ).flow
            .map { pagingData -> pagingData.map { it.toDomainModel() } }
    }

    override fun get(pagingConfig: PagingConfig?): Flow<PagingData<Client>> {
        val pagingSourceFactory = { ClientPagingSource(dao) }
        return Pager(
            config = pagingConfig ?: getDefaultPageConfig(),
            pagingSourceFactory = pagingSourceFactory
        ).flow
            .map { pagingData -> pagingData.map { it.toDomainModel() } }
    }
}

class ClientPagingSource(
    private val dao: ClientDao
) : PagingSource<Int, my.net.diary.db.Client>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, my.net.diary.db.Client> {
        return try {
            val pageNumber = params.key ?: 0
            val prevKey = if (pageNumber > 0) pageNumber - 1 else null
            val data = dao.get((params.loadSize * pageNumber).toLong(), params.loadSize)
            val nextKey = if (data.isNotEmpty()) pageNumber + 1 else null
            LoadResult.Page(
                data = data,
                prevKey = prevKey,
                nextKey = nextKey
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, my.net.diary.db.Client>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }
}