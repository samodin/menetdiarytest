package my.net.diary.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Client::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun clientDao(): ClientDao

}