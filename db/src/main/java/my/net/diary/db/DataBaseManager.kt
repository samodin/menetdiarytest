package my.net.diary.db

import android.content.Context
import androidx.room.Room

const val DATABASE_NAME = "client_database.db"

class DataBaseManager(private val context: Context) {
    private val db: AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()

    fun getClientDao(): ClientDao {
        return db.clientDao()
    }
}