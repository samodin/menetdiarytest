package my.net.diary.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = ClientDao.TABLE_NAME)
class Client(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "weight") val weight: Double,
    @ColumnInfo(name = "weight_unit") val weightUnit: String,
    @ColumnInfo(name = "date_of_birth") val dateOfBirth: Long,
    @ColumnInfo(name = "photo_url") val photoUrl: String
)